package com.hepta.mercado.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Produto implements Serializable {
	private static final long serialVersionUID = 1L;

	public Produto() {
	}

	public Produto(Integer id, String nome, Fabricante fabricante, Double volume, String unidade, Integer estoque) {
		this.id = id;
		this.nome = nome;
		this.fabricante = fabricante;
		this.volume = volume;
		this.unidade = unidade;
		this.estoque = estoque;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PRODUTO")
	private Integer id;

	@Column(name = "NOME")
	private String nome;

	@ManyToOne
	@JoinColumn(name = "ID_FABRICANTE")
	private Fabricante fabricante;

	@Column(name = "VOLUME")
	private Double volume;

	@Column(name = "UNIDADE")
	private String unidade;

	@Column(name = "ESTOQUE")
	private Integer estoque;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Fabricante getFabricante() {
		return fabricante;
	}

	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

	public Integer getEstoque() {
		return estoque;
	}

	public void setEstoque(Integer estoque) {
		this.estoque = estoque;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((estoque == null) ? 0 : estoque.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((unidade == null) ? 0 : unidade.hashCode());
		result = prime * result + ((volume == null) ? 0 : volume.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (estoque == null) {
			if (other.estoque != null)
				return false;
		} else if (!estoque.equals(other.estoque))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (unidade == null) {
			if (other.unidade != null)
				return false;
		} else if (!unidade.equals(other.unidade))
			return false;
		if (volume == null) {
			if (other.volume != null)
				return false;
		} else if (!volume.equals(other.volume))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Produto [id=" + id + ", nome=" + nome + ", fabricante=" + fabricante + ", volume=" + volume
				+ ", unidade=" + unidade + ", estoque=" + estoque + "]";
	}

	public static class ProdutoBuilder {
		private Integer id;
		private String nome;
		private Fabricante fabricante;
		private Double volume;
		private String unidade;
		private Integer estoque;

		public ProdutoBuilder comId(Integer id) {
			this.id = id;
			return this;
		}

		public ProdutoBuilder comNome(String nome) {
			this.nome = nome;
			return this;
		}

		public ProdutoBuilder comFabricante(Fabricante fabricante) {
			this.fabricante = fabricante;
			return this;
		}

		public ProdutoBuilder comVolume(Double volume) {
			this.volume = volume;
			return this;
		}

		public ProdutoBuilder comUnidade(String unidade) {
			this.unidade = unidade;
			return this;
		}

		public ProdutoBuilder comEstoque(Integer estoque) {
			this.estoque = estoque;
			return this;
		}

		public Produto build() {
			if(id == null) {
				return new Produto(null, nome, fabricante, volume, unidade, estoque);
			}
			return new Produto(id, nome, fabricante, volume, unidade, estoque);
		}
	}
}

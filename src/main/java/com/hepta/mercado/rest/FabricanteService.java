package com.hepta.mercado.rest;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.RollbackException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.hepta.mercado.entity.Fabricante;
import com.hepta.mercado.persistence.FabricanteDAO;

@Path("/fabricantes")
public class FabricanteService {

	private final static String JSON_IMPOSSIVEL_DELETAR = "{\"error\":\"N�o � possivel excluir pois existem produtos associados\"}";
	@Context
	private HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	private FabricanteDAO dao;

	public FabricanteService() {
		dao = new FabricanteDAO();
	}

	protected void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	/**
	 * Adiciona novo fabricante no mercado
	 * 
	 * @param fabricante: Novo fabricante
	 * @return response 201 (Created) - Conseguiu adicionar
	 */
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@POST
	public Response fabricanteCreate(Fabricante fabricante) {
		URI uri = null;
		try {
			Fabricante fabricanteCriado = dao.save(fabricante);
			uri = URI.create("fabricantes/" + fabricanteCriado.getId().toString());
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao criar fabricante").build();
		}
		return Response.created(uri).build();
	}

	/**
	 * Lista todos os fabricantes do mercado
	 * 
	 * @return response 200 (OK) - Conseguiu listar
	 */
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public Response fabricanteRead() {
		List<Fabricante> fabricantes = new ArrayList<>();
		try {
			fabricantes = dao.getAll();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar fabricantes").build();
		}
		GenericEntity<List<Fabricante>> entity = new GenericEntity<List<Fabricante>>(fabricantes) {
		};
		return Response.status(Status.OK).entity(entity).build();
	}

	/**
	 * Busca por Id
	 * 
	 * @return response 200 (OK) - Conseguiu buscar
	 */
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public Response fabricanteFind(@PathParam("id") Integer id) {
		Fabricante fabricante = null;
		try {
			fabricante = dao.find(id);
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar fabricantes").build();
		}

		GenericEntity<Fabricante> entity = new GenericEntity<Fabricante>(fabricante) {
		};
		return Response.status(Status.OK).entity(entity).build();
	}

	/**
	 * Atualiza um fabricante no mercado
	 * 
	 * @param id:         id do fabricante
	 * @param fabricante: Fabricante atualizado
	 * @return response 200 (OK) - Conseguiu atualiza
	 */
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@PUT
	public Response fabricanteUpdate(@PathParam("id") Integer id, Fabricante fabricante) {
		try {
			Fabricante find = dao.find(id);
			if (find == null) {
				return Response.status(Status.NOT_FOUND).build();
			}
			fabricante.setId(id);
			dao.update(fabricante);
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar fabricante").build();
		}
		GenericEntity<Fabricante> entity = new GenericEntity<Fabricante>(fabricante) {
		};
		return Response.status(Status.OK).entity(entity).build();
	}

	/**
	 * Remove um fabricante do mercado
	 * 
	 * @param id: id do fabricante
	 * @return response 204 (No content) - Conseguiu remover
	 */
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@DELETE
	public Response fabricanteDelete(@PathParam("id") Integer id) {
		Fabricante find;
		try {
			find = dao.find(id);
			if (find == null) {
				return Response.status(Status.NOT_FOUND).build();
			}
			dao.delete(id);
		} catch (RollbackException e) {
			return Response.status(Status.FORBIDDEN).entity(JSON_IMPOSSIVEL_DELETAR).build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao deletar fabricante").build();
		}
		return Response.status(Status.NO_CONTENT).build();
	}
}

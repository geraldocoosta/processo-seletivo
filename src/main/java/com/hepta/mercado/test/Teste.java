package com.hepta.mercado.test;

import java.util.List;

import com.hepta.mercado.entity.Fabricante;
import com.hepta.mercado.entity.Produto;
import com.hepta.mercado.persistence.jdbc.dao.FabricanteDAO;
import com.hepta.mercado.persistence.jdbc.dao.ProdutoDAO;

public class Teste {

	public static void main(String[] args) {
		FabricanteDAO fabricanteDao = new FabricanteDAO();
		Fabricante fabricante = new Fabricante(null, "Uma fabricante incluida " + System.currentTimeMillis());

		/* salvar */
		boolean save = fabricanteDao.save(fabricante);
		System.out.println(save ? "salvou" : "erro");
		System.out.println();

		/* achar */
		Fabricante fabricanteAchado = fabricanteDao.find(1);
		System.out.println(fabricanteAchado);
		System.out.println();

		/* update */
		fabricanteAchado.setNome("Uma fabricante mudada " + System.currentTimeMillis());
		fabricanteDao.update(fabricanteAchado);
		fabricanteAchado = fabricanteDao.find(1);
		System.out.println(fabricanteAchado);
		System.out.println();

		/* listar todos */
		List<Fabricante> todosFabricantes = fabricanteDao.getAll();
		todosFabricantes.stream().forEach(System.out::println);
		System.out.println();

		Fabricante fabricanteDeletado = todosFabricantes.get(todosFabricantes.size() - 1);
		fabricanteDao.delete(fabricanteDeletado.getId());
		System.out.println("se chegou deletou");
		System.out.println();

		todosFabricantes = fabricanteDao.getAll();
		todosFabricantes.stream().forEach(System.out::println);

		/* código de testes, esqueci a oo aqui xD */
		System.out.println();
		System.out.println();
		/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

		ProdutoDAO produtoDao = new ProdutoDAO();

		List<Produto> todosProdutos = produtoDao.getAll();
		todosProdutos.stream().forEach(System.out::println);
		System.out.println();

		Produto produto = produtoDao.find(12);
		System.out.println(produto);
		System.out.println();

		produto.setNome("Geraldo productions " + System.currentTimeMillis());
		produtoDao.update(produto);
		Produto produtoAtualizado = produtoDao.find(12);
		System.out.println(produtoAtualizado);

		Produto produtoInserido = new Produto.ProdutoBuilder().comNome("Produto " + System.currentTimeMillis())
				.comFabricante(fabricanteAchado).comUnidade("UNIDADE " + System.currentTimeMillis()).comVolume(29.5)
				.comEstoque(82).build();

		System.out.println();
//		boolean isProdutoSalvo = produtoDao.save(produtoInserido);
//		System.out.println(isProdutoSalvo ? "salvou" : "não deu");
		System.out.println();

		Produto produtoExcluido = todosProdutos.get(todosProdutos.size() - 1);
		produtoDao.delete(produtoExcluido.getId());
		System.out.println("Chegou aqui deu bom");
		System.out.println();

		todosProdutos = produtoDao.getAll();
		todosProdutos.stream().forEach(System.out::println);

	}
}

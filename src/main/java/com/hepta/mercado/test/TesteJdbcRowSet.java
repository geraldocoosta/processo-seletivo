package com.hepta.mercado.test;

import java.util.List;

import com.hepta.mercado.entity.Fabricante;
import com.hepta.mercado.entity.Produto;
import com.hepta.mercado.persistence.jdbc.dao.FabricanteDAO;
import com.hepta.mercado.persistence.jdbc.dao.ProdutoDAO;

public class TesteJdbcRowSet {
	public static void main(String[] args) {
		ProdutoDAO produtoDao = new ProdutoDAO();
		FabricanteDAO fabricanteDao = new FabricanteDAO();

		List<Fabricante> todosFabricantes = fabricanteDao.getAll();
		Fabricante fabricante = todosFabricantes.get(todosFabricantes.size() - 1);
		
		Produto produto = new Produto.ProdutoBuilder()
			.comFabricante(fabricante)
			.comNome("Geraldo produto " + System.currentTimeMillis())
			.comEstoque(54)
			.comUnidade("Gama")
			.comVolume(54.8)
			.build();
		
		produtoDao.save(produto, false);
	}
}

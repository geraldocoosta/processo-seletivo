package com.hepta.mercado.persistence.jdbc.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetProvider;

public class FabricaConexao {

	private static final String URL = "jdbc:mysql://localhost:3306/mercado?useTimezone=true&serverTimezone=UTC&useSSL=false";
	private static final String USER = "root";
	private static final String PW = "";

	private FabricaConexao() {
	}

	public static Connection getConexao() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
			return DriverManager.getConnection(URL, USER, PW);
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public static JdbcRowSet getRowSetConnection() {
		String url = "jdbc:mysql://localhost:3306/mercado?useTimezone=true&serverTimezone=UTC&useSSL=false";
		String user = "root";
		String password = "root";
		try {
			JdbcRowSet jdbcRowSet = RowSetProvider.newFactory().createJdbcRowSet();
			jdbcRowSet.setUrl(url);
			jdbcRowSet.setUsername(user);
			jdbcRowSet.setPassword(password);
			return jdbcRowSet;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void fecharConexao(Connection conn) {
		try {
			if (conn != null)
				conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void fecharConexao(Connection conn, Statement s) {
		fecharConexao(conn);
		try {
			if (s != null)
				s.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void fecharConexao(Connection conn, Statement s, ResultSet rs) {
		fecharConexao(conn, s);
		try {
			if (rs != null)
				rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void fecharConexao(ResultSet rs) {
		try {
			if (rs != null)
				rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}

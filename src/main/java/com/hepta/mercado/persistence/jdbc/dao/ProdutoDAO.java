package com.hepta.mercado.persistence.jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.hepta.mercado.entity.Fabricante;
import com.hepta.mercado.entity.Produto;
import com.hepta.mercado.persistence.jdbc.util.FabricaConexao;

public class ProdutoDAO {
	
	private static final String SQL_SELECT = "select ID_PRODUTO, NOME, ESTOQUE, UNIDADE, VOLUME, ID_FABRICANTE from produto";

	public boolean save(Produto produto) {
		String sql = "insert into produto (ESTOQUE, NOME, UNIDADE, VOLUME, ID_FABRICANTE) values (?, ?, ?, ?, ?)";

		try (Connection conn = FabricaConexao.getConexao(); PreparedStatement ps = conn.prepareStatement(sql)) {

			
			ps.setInt(1, produto.getEstoque());
			ps.setString(2, produto.getNome());
			ps.setString(3, produto.getUnidade());
			ps.setDouble(4, produto.getVolume());
			ps.setInt(5, produto.getFabricante().getId());
			
			int linhasAfetadas = ps.executeUpdate();
			if (linhasAfetadas == 0) {
				throw new RuntimeException();
			}
			return true;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public Produto save(Produto produto, Boolean miau) {
		try (JdbcRowSet jrs = FabricaConexao.getRowSetConnection()) {
			jrs.setCommand(SQL_SELECT);
			jrs.execute();
			
			jrs.moveToInsertRow();
			jrs.updateString("NOME", produto.getNome());
			jrs.updateString("UNIDADE", produto.getUnidade());
			jrs.updateFloat("VOLUME", produto.getVolume().floatValue());
			jrs.updateInt("ESTOQUE", produto.getEstoque());
			jrs.updateInt("ID_FABRICANTE", produto.getFabricante().getId());
			
			jrs.insertRow();
			jrs.last();
			
			produto.setId(jrs.getInt("ID_PRODUTO"));
			
			return produto;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}

	public Produto update(Produto produto) {
		if (produto == null || produto.getId() == null || produto.getId() <= 0) {
			throw new IllegalArgumentException();
		}
		String sqlUpdate = "update produto set NOME = ?, ESTOQUE = ?, UNIDADE = ?, VOLUME = ?, ID_FABRICANTE= ? where ID_PRODUTO = ?";
		try (Connection conn = FabricaConexao.getConexao(); PreparedStatement ps = conn.prepareStatement(sqlUpdate)) {

			ps.setString(1, produto.getNome());
			ps.setInt(2, produto.getEstoque());
			ps.setString(3, produto.getUnidade());
			ps.setDouble(4, produto.getVolume());
			ps.setInt(5, produto.getFabricante().getId());
			ps.setInt(6, produto.getId());
			
			int linhasAfetadas = ps.executeUpdate();
			if (linhasAfetadas > 0) {
				return produto;
			} else {
				throw new RuntimeException();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void delete(Integer id) {
		if (idIdValido(id)) {
			throw new IllegalArgumentException();
		}
		String sqlDelete = "delete from produto where ID_PRODUTO = ?";
		try (Connection conn = FabricaConexao.getConexao(); PreparedStatement ps = conn.prepareStatement(sqlDelete)) {
			ps.setInt(1, id);
			int linhasAfetadas = ps.executeUpdate();
			if (linhasAfetadas == 0) {
				throw new RuntimeException();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public Produto find(Integer id) {
		if (idIdValido(id)) {
			throw new IllegalArgumentException();
		}

		ResultSet rs = null;
		Produto produto = null;
		String sqlSelect = "SELECT a.ID_PRODUTO, a.ESTOQUE, a.NOME, a.UNIDADE, a.VOLUME, a.ID_FABRICANTE, b.nome as 'NOME_FABRICANTE' "
				+ "FROM produto a inner JOIN fabricante b on a.ID_FABRICANTE = b.ID_FABRICANTE WHERE a.ID_PRODUTO = ?";

		try (Connection conn = FabricaConexao.getConexao(); PreparedStatement ps = conn.prepareStatement(sqlSelect)) {
			ps.setInt(1, id);
			rs = ps.executeQuery();
			
			rs.first();
			
			int idFabricante = rs.getInt("ID_FABRICANTE");
			String nomeFabricante = rs.getString("NOME_FABRICANTE");

			Fabricante fabricante = new Fabricante(idFabricante,nomeFabricante);
			
			produto = new Produto.ProdutoBuilder()
					.comId(rs.getInt("ID_PRODUTO"))
					.comNome(rs.getString("NOME"))
					.comEstoque(rs.getInt("ESTOQUE"))
					.comUnidade(rs.getString("UNIDADE"))
					.comVolume(rs.getDouble("VOLUME"))
					.comFabricante(fabricante)
					.build();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			FabricaConexao.fecharConexao(rs);
		}

		return produto;
	}

	public List<Produto> getAll() {
		ResultSet rs = null;
		List<Produto> produtos = new ArrayList<>();
		String sqlSelect = "SELECT a.ID_PRODUTO, a.ESTOQUE, a.NOME, a.UNIDADE, a.VOLUME, a.ID_FABRICANTE, b.nome as 'NOME_FABRICANTE' "
				+ "FROM produto a inner JOIN fabricante b on a.ID_FABRICANTE = b.ID_FABRICANTE";

		try (Connection conn = FabricaConexao.getConexao();
				PreparedStatement ps = conn.prepareStatement(sqlSelect)) {
			rs = ps.executeQuery();

			while (rs.next()) {
				int idFabricante = rs.getInt("ID_FABRICANTE");
				String nomeFabricante = rs.getString("NOME_FABRICANTE");
				
				Fabricante fabricante = new Fabricante(idFabricante, nomeFabricante);
				
				produtos.add(new Produto.ProdutoBuilder()
						.comId(rs.getInt("ID_PRODUTO"))
						.comNome(rs.getString("NOME"))
						.comEstoque(rs.getInt("ESTOQUE"))
						.comUnidade(rs.getString("UNIDADE"))
						.comVolume(rs.getDouble("VOLUME"))
						.comFabricante(fabricante)
						.build());
			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			FabricaConexao.fecharConexao(rs);
		}
		return produtos;
	}
	

	private boolean idIdValido(Integer id) {
		return id == null || id < 0;
	}

}

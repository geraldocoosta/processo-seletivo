package com.hepta.mercado.persistence.jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.hepta.mercado.entity.Fabricante;
import com.hepta.mercado.persistence.jdbc.util.FabricaConexao;

public class FabricanteDAO {

	private static final String SQL_SELECT = "select ID_FABRICANTE, NOME from fabricante";

	public boolean save(Fabricante fabricante) {
		String sql = "INSERT INTO fabricante (ID_FABRICANTE, NOME) VALUES (NULL, ?)";

		try (Connection conn = FabricaConexao.getConexao(); PreparedStatement ps = conn.prepareStatement(sql)) {

			ps.setString(1, fabricante.getNome());
			int linhasAfetadas = ps.executeUpdate();
			if (linhasAfetadas == 0) {
				throw new RuntimeException();
			}
			return true;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public Fabricante save(Fabricante fabricante, boolean miau) {
		try (JdbcRowSet jrs = FabricaConexao.getRowSetConnection()) {
			jrs.setCommand(SQL_SELECT);
			jrs.execute();

			jrs.moveToInsertRow();
			jrs.updateString("NOME", fabricante.getNome());

			jrs.insertRow();
			jrs.last();

			fabricante.setId(jrs.getInt("ID_FABRICANTE"));

			return fabricante;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public Fabricante update(Fabricante fabricante) {
		if (fabricante == null || fabricante.getId() == null || fabricante.getId() <= 0) {
			throw new IllegalArgumentException();
		}
		String sqlUpdate = "update fabricante set nome = ? where ID_FABRICANTE = ?";
		try (Connection conn = FabricaConexao.getConexao(); PreparedStatement ps = conn.prepareStatement(sqlUpdate)) {

			ps.setString(1, fabricante.getNome());
			ps.setInt(2, fabricante.getId());
			int linhasAfetadas = ps.executeUpdate();
			if (linhasAfetadas > 0) {
				return fabricante;
			} else {
				throw new RuntimeException();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void delete(Integer id) {
		if (idIdValido(id)) {
			throw new IllegalArgumentException();
		}
		String sqlDelete = "delete from fabricante where ID_FABRICANTE = ?";
		try (Connection conn = FabricaConexao.getConexao(); PreparedStatement ps = conn.prepareStatement(sqlDelete)) {
			ps.setInt(1, id);
			int linhasAfetadas = ps.executeUpdate();
			if (linhasAfetadas == 0) {
				throw new RuntimeException();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public Fabricante find(Integer id) {
		if (idIdValido(id)) {
			throw new IllegalArgumentException();
		}

		ResultSet rs = null;
		Fabricante fabricante = null;

		String sqlSelect = "select ID_FABRICANTE, NOME from fabricante where ID_FABRICANTE = ?";
		try (Connection conn = FabricaConexao.getConexao(); PreparedStatement ps = conn.prepareStatement(sqlSelect)) {
			ps.setInt(1, id);
			rs = ps.executeQuery();
			rs.next();
			fabricante = new Fabricante(rs.getInt("ID_FABRICANTE"), rs.getString("nome"));
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			FabricaConexao.fecharConexao(rs);
		}

		return fabricante;
	}

	public List<Fabricante> getAll() {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Fabricante> fabricantes = new ArrayList<>();

		try {
			conn = FabricaConexao.getConexao();
			ps = conn.prepareStatement(SQL_SELECT);
			rs = ps.executeQuery();

			while (rs.next()) {
				fabricantes.add(new Fabricante(rs.getInt("ID_FABRICANTE"), rs.getString("nome")));
			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			FabricaConexao.fecharConexao(conn, ps, rs);
		}
		return fabricantes;
	}

	private boolean idIdValido(Integer id) {
		return id == null || id < 0;
	}

}

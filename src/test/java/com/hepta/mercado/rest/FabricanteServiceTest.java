package com.hepta.mercado.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.net.URI;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.hepta.mercado.entity.Fabricante;

class FabricanteServiceTest {
	private static WebTarget service;
	private static final String URL_LOCAL = "http://localhost:8080/mercado/rs/fabricantes";
	private static Client client;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ClientConfig config = new ClientConfig();
		client = ClientBuilder.newClient(config);
		service = client.target(UriBuilder.fromUri(URL_LOCAL).build());
	}

	@Test
	void testListaTodosFabricantes() {
		// QUANDO
		Response response = service.request().get();
		
		System.out.println(response.getStatus());
		// ENTAO
		assertTrue(response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode());
	}

	@Test
	void testaQuandoIncluidoFabricanteNovo() {
		// MONTANDO PRODUTO
		Fabricante fabricante = criaFabricante("Fabricante1");
		Entity<Fabricante> entity = Entity.entity(fabricante, MediaType.APPLICATION_JSON);

		// QUANDO INSERINDO O PRODUTO
		Response response = service.request().post(entity);
		// ENTAO
		assertTrue(response.getStatus() == Response.Status.CREATED.getStatusCode());

		// Testando se o fabricante que o header location aponta � o mesmo que foi incluido
		URI localizacaoFabricante = response.getLocation();
		WebTarget fabricanteIncluidoTarget = client.target(localizacaoFabricante);

		Response responseFabricanteIncluido = fabricanteIncluidoTarget.request().get();
		Fabricante fabricanteIncluido = responseFabricanteIncluido.readEntity(Fabricante.class);

		assertEquals(fabricante, fabricanteIncluido);
	}

	@Test
	void testandoQuandoFabricanteAlterado() {
		String nomeFabricante = "Fabricante2";

		// testando quando alterado um fabricante
		Fabricante fabricante = criaFabricante(nomeFabricante );
		Response response = service.path("/1").request().put(Entity.entity(fabricante, MediaType.APPLICATION_JSON));

		// verificando se status code � o correto
		assertTrue(response.getStatus() == Response.Status.OK.getStatusCode());

		Fabricante fabricanteAlterado = response.readEntity(Fabricante.class);

		// verificando se as altera��es foram realmente persistidas
		assertTrue(fabricanteAlterado.getNome().equals(nomeFabricante));

	}

	@Test
	void testandoQuandoTentarAlterarIdNaoExistente() {
		Fabricante fabricante = criaFabricante("Fabricante");
		// verificando status code de uma modifica��o em um fabricante n�o incluido
		Response response = service.path("/789789").request().put(Entity.entity(fabricante, MediaType.APPLICATION_JSON));

		assertTrue(response.getStatus() == Response.Status.NOT_FOUND.getStatusCode());
	}

	@Test
	void testandoQuandoRegistroApagado() {
		// buscando todos fabricantes
		List<Fabricante> listaFabricantes = service.request().get().readEntity(new GenericType<List<Fabricante>>() {
		});

		// se a lista tiver vazia da erro
		if (listaFabricantes.size() == 0) {
			fail("Lista vazia, por favor incluir registro na lista de fabricantes");
		}

		// pegando ultimo fabricante
		Fabricante fabricante = listaFabricantes.get(listaFabricantes.size() - 1);

		// deletando
		Response response = service.path("/" + fabricante.getId()).request().delete();

		// verificando se o status code � o correto
		assertTrue(response.getStatus() == Response.Status.NO_CONTENT.getStatusCode());
	}

	private Fabricante criaFabricante(String nome) {
		Fabricante fabricante = new Fabricante();
		fabricante.setNome(nome);
		return fabricante;
	}
}

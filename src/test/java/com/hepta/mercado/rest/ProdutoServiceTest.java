package com.hepta.mercado.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.net.URI;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.hepta.mercado.entity.Fabricante;
import com.hepta.mercado.entity.Produto;

class ProdutoServiceTest {

	private static WebTarget service;
	private static final String URL_LOCAL = "http://localhost:8080/mercado/rs/produtos";
	private static Client client;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ClientConfig config = new ClientConfig();
		client = ClientBuilder.newClient(config);
		service = client.target(UriBuilder.fromUri(URL_LOCAL).build());
	}

	@Test
	void testListaTodosProdutos() {
		// QUANDO
		Response response = service.request().get();
		// ENTAO
		assertTrue(response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode());
	}

	@Test
	void testaQuandoIncluidoProdutoNovo() {
		// MONTANDO PRODUTO
		Produto produto = criaProduto("Produto1", 45);
		Entity<Produto> entity = Entity.entity(produto, MediaType.APPLICATION_JSON);

		// QUANDO INSERINDO O PRODUTO
		Response response = service.request().post(entity);
		// ENTAO
		assertTrue(response.getStatus() == Response.Status.CREATED.getStatusCode());

		// Testando se o produto que o header location aponta � o mesmo que foi incluido
		URI localizacaoProduto = response.getLocation();
		WebTarget produtoIncluidoTarget = client.target(localizacaoProduto);

		Response responseProdutoIncluido = produtoIncluidoTarget.request().get();
		Produto produtoIncluido = responseProdutoIncluido.readEntity(Produto.class);

		assertEquals(produto, produtoIncluido);
	}

	@Test
	void testandoQuandoProdutoAlterado() {
		String nomeProduto = "Produto2";
		int qtdProduto = 22;

		//testando quando alterado um produto
		Produto produto = criaProduto(nomeProduto, qtdProduto);
		Response response = service.path("/1").request().put(Entity.entity(produto, MediaType.APPLICATION_JSON));

		// verificando se status code � o correto
		assertTrue(response.getStatus() == Response.Status.OK.getStatusCode());

		Produto produtoAlterado = response.readEntity(Produto.class);

		// verificando se as altera��es foram realmente persistidas
		assertTrue(produtoAlterado.getNome().equals(nomeProduto));
		assertEquals(qtdProduto, produtoAlterado.getEstoque());

	}

	@Test
	void testandoQuandoTentarAlterarIdNaoExistente() {
		Produto produto = criaProduto("Produto", 15);
		// verificando status code de uma modifica��o em um produto n�o incluido
		Response response = service.path("/789789").request().put(Entity.entity(produto, MediaType.APPLICATION_JSON));

		assertTrue(response.getStatus() == Response.Status.NOT_FOUND.getStatusCode());
	}

	@Test
	void testandoQuandoRegistroApagado() {
		// buscando todos produtos
		List<Produto> listaProdutos = service.request().get().readEntity(new GenericType<List<Produto>>() {
		});
		
		// se a lista tiver vazia da erro
		if (listaProdutos.size() == 0) {
			fail("Lista vazia, por favor incluir registro na lista de produtos");
		}
		
		//pegando ultimo produto
		Produto produto = listaProdutos.get(listaProdutos.size() - 1);

		// deletando
		Response response = service.path("/" + produto.getId()).request().delete();

		// verificando se o status code � o correto
		assertTrue(response.getStatus() == Response.Status.NO_CONTENT.getStatusCode());
	}
	

	private Produto criaProduto(String nome, Integer estoque) {
		Produto produto = new Produto();
		produto.setNome(nome);
		produto.setEstoque(estoque);
		produto.setFabricante(new Fabricante(1, "teste"));
		produto.setUnidade("unidade");
		produto.setVolume(25.5);
		return produto;
	}

}

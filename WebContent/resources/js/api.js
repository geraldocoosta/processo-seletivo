let api = axios.create({
  baseURL: 'http://localhost:8080/mercado/rs'
});

// Realizando consumo de api com XMLHTTPREQUEST //

let callbackPadrao = () => {};

const getUrl = function getURL(path, method, callback = callbackPadrao, sendArguments, contentType = 'application/x-www-form-urlencoded') {
    let req = new XMLHttpRequest();
   
    req.open(method, path);

    req.addEventListener('readystatechange', function () {
     const status = req.status;
      if ((status === 200 || status === 204 || status === 201) && req.readyState === 4)
        callback(req.responseText);
      else if (status === 400 || status === 500 || status === 403 || status === 404)
        callback(null, new Error("Request failed" + req.statusText));
    }, false);

    req.addEventListener('error', function () {
      callback(null, new Error("Network error"));
    }, false);

    if (method !== 'GET') {
      req.setRequestHeader('Content-Type', contentType);
    }
    
    req.send(sendArguments);
 }

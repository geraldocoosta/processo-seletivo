const routes = [{
		path: '/',
		name: 'produtos',
		component: CadastroProduto,
		menu: true,
		titulo: 'Produtos'
	},
	{
		path: '/cadastro-fabricante',
		name: 'fabricante',
		component: CadastroFabricante,
		menu: true,
		titulo: 'Fabricantes'
	}
]

const router = new VueRouter({
	routes
})

new Vue({
	el: "#inicio",
	router,
	data() {
		return {
			routes: routes.filter(route => route.menu)
		}
	},
	template: `
		<div class="full-height">
			<componente-menu :rotas="routes"></componente-menu>
			<div class="container full-height">
				<router-view></router-view>
			</div>
		</div>`
});
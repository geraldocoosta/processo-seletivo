let produtosService = {
  lista() {
    return api.get('/produtos')
      .then(res => res.data);
  },
  cadastra(produto) {
	if(produto.id){
		return api.put(`/produtos/${produto.id}`, produto)
		.then((res) => res.data);
	}
    return api.post('/produtos', produto)
      .then((res) => res.data);
  },
  apaga(id) {
	  return api.delete(`/produtos/${id}`,)
      	.then((res) => res.data);
  },

  busca(id) {
	  return api.get(`/produtos/${id}`)
	  	.then((res) => res.data);
  }
}

/*  REALIZANDO CONSUMO DE SERVIÇO VIA XMLHTTPREQUEST */

const produtoServiceComUrl = {
	baseURL: 'http://localhost:8080/mercado/rs',
	contentType: 'application/json',
	dominio: 'produtosJdbc',
	lista(callback) {
		getUrl(`${this.baseURL}/${this.dominio}`, 'GET', callback);
	},
	cadastra(callback, produto){
		if(produto.id){
			getUrl(`${this.baseURL}/${this.dominio}/${produto.id}`, 'PUT', callback, JSON.stringify(produto), this.contentType);
			return;
		}
		getUrl(`${this.baseURL}/${this.dominio}`, 'POST', callback, JSON.stringify(produto), this.contentType);
	},
	apaga(id, callback){
		getUrl(`${this.baseURL}/${this.dominio}/${id}`, 'DELETE', callback);
	},
	busca(id, callback){
		getUrl(`${this.baseURL}/${this.dominio}/${id}`, 'GET', callback);
	}
}


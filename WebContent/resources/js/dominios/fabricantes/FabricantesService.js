let fabricanteService = {
  lista() {
    return api.get('/fabricantes')
      .then(res => res.data);
  },
  cadastra(fabricante) {
		if(fabricante.id){
			return api.put(`/fabricantes/${fabricante.id}`, fabricante)
			.then((res) => res.data);
		}
	  return api.post('/fabricantes', fabricante)
	  .then(res => res.data);
  },
  apaga(id) {
	  return api.delete(`/fabricantes/${id}`);
  },
  busca(id) {
	  return api.get(`/fabricantes/${id}`)
	  	.then((res) => res.data);
  }
};

/*  REALIZANDO CONSUMO DE SERVIÇO VIA XMLHTTPREQUEST */

const fabricantesServiceComUrl = {
		baseURL: 'http://localhost:8080/mercado/rs',
		contentType: 'application/json',
		dominio: 'fabricantesJdbc',
		lista(callback) {
			getUrl(`${this.baseURL}/${this.dominio}`, 'GET', callback);
		},
		cadastra(callback, fabricante){
			console.log(fabricante);
			if(fabricante.id){
				getUrl(`${this.baseURL}/${this.dominio}/${fabricante.id}`, 'PUT', callback, JSON.stringify(fabricante), this.contentType);
				return;
			}
			getUrl(`${this.baseURL}/${this.dominio}`, 'POST', callback, JSON.stringify(fabricante), this.contentType);
		},
		apaga(id, callback){
			getUrl(`${this.baseURL}/${this.dominio}/${id}`, 'DELETE', callback);
		},
		busca(id, callback){
			getUrl(`${this.baseURL}/${this.dominio}/${id}`, 'GET', callback);
		}
	}


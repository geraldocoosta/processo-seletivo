let ComponentMenu = Vue.component('componente-menu', {
	props: ['rotas'],
	template: `
  <nav class="navbar navbar-expand-mercado navbar-light bg-light">
    <router-link class="navbar-brand" :to="{ name:'produtos'}">Mercadinho</router-link>

    <div class="collapse navbar-collapse" id="conteudoNavbarSuportado">
      <ul class="navbar-nav mr-auto">
        <li v-for="(rota, index) in rotas" :key="index" class="nav-item">
          <router-link tag="li" :to="rota.path ? rota.path : '/'" class="nav-link">
            {{ rota.titulo }}
          </router-link>
        </li>
      </ul>
    </div>
  </nav>
	`
});

let FormCadastraFabricante = Vue.component('form-cadastra-fabricante', {
	props: [ 'limparForm', 'fabricanteAlterar' ],
	created(){
		if (this.limparForm) {
			this.fabricante = {
				id: '',
				nome: '',
			}
		}
		if( this.fabricanteAlterar){
			this.fabricante = this.fabricanteAlterar;
		}
	},
	data() {
		return{
			fabricante:{
				nome: '',
				id: '',
			}
		}
	},
	methods:{
		grava(){
			fabricantesServiceComUrl
				.cadastra(() => this.$emit('posGravacao'), this.fabricante);
		},
		fecharForm(){
			this.fabricante = {
				id: '',
				nome: '',				
			};
			this.$emit('fecharCadastro');
		}
	},
	template: `
	<form class="mt-2" @submit.prevent="grava()">
		<form-input 
			htmlFor="fabricante-nome" 
			type="text" 
			label="Nome" 
			placeholder="insira o nome do Fabricante" 
			:valorInput="fabricante.nome" 
			@retornaValor="($event) => this.fabricante.nome = $event" 
		/>
		<button class="btn btn-primary" type="submit"> Gravar </button>
		<button class="btn btn-danger" type="reset" @click="fecharForm()">Fechar Cadastro</button>
	</form>
	`,
});

let FormCadastraProduto = Vue.component('form-cadastra-produto', {
	props: ['limparForm', 'produtoAlterar'],
	created() {
		if (this.limparForm) {
			this.produto = {
				id: '',
				nome: '',
				fabricante: {},
				volume: '',
				unidade: '',
				estoque: '',
			}
		}
		if (this.produtoAlterar) {
			this.produto = this.produtoAlterar;
		}
	},
	data() {
		return {
			produto: {
				id: '',
				nome: '',
				fabricante: {},
				volume: '',
				unidade: '',
				estoque: '',
			},
			valid: true,
		}
	},
	methods: {
		grava() {
			produtoServiceComUrl
				.cadastra(() => this.$emit('posGravacao'), this.produto);
		},
		setFabricante($event) {
			this.produto.fabricante = $event;
		},
		fecharForm() {
			this.produto = {
				id: '',
				nome: '',
				fabricante: {},
				volume: '',
				unidade: '',
				estoque: '',
			};
			this.$emit('fecharCadastro');
		}
	},
	template: `
		<div>
		<form class="mt-2" @submit.prevent="grava()">
			<form-input 
				htmlFor="produto-nome" 
				type="text" 
				label="Nome" 
				:required="true"
				placeholder="insira o nome do Produto" 
				:valorInput="produto.nome" 
				@retornaValor="($event) => this.produto.nome = $event" 
			/>
			<form-input 
				htmlFor="produto-volume" 
				type="number" 
				label="Volume" 
				:required="true"
				placeholder="Insira o volume" 
				:valorInput="produto.volume" 
				@retornaValor="($event) => this.produto.volume = $event" 
			/>
			<form-input 
				htmlFor="produto-unidade" 
				type="text" 
				label="Unidade" 
				:required="true"
				placeholder="Insira a unidade" 
				:valorInput="produto.unidade" 
				@retornaValor="($event) => this.produto.unidade = $event" 
			/>
			<form-input 
				htmlFor="produto-estoque" 
				type="number" 
				label="Estoque"
				:required="true" 
				placeholder="Insira o Estoque" 
				:valorInput="produto.estoque" 
				@retornaValor="($event) => this.produto.estoque = $event" 
			/>
			<form-select-fabricantes 
			  htmlFor="produto-fabricante" 
			  label="Fabricante" 
			  :required="true"
				labelFirstItem="Escolha um fabricante"
				:valor="produto.fabricante" 
			  @retornaValor="setFabricante($event)" 
			/>
			<button class="btn btn-primary" type="submit"> Gravar </button>
			<button class="btn btn-danger" type="reset" @click="fecharForm()">Fechar Cadastro</button>
		</form>
		  <b-alert variant="danger" :show="!valid" dismissible>
			Preencha todos os valores
		  </b-alert>
		</div>
		`
});

let FormSelectFabricantes = Vue.component('form-select-fabricantes', {
	props: ['label', 'labelFirstItem', 'htmlFor', 'valor'],
	data() {
		const {
			id,
			nome
		} = this.valor;
		let auxValor = null;

		if (id) {
			auxValor = {
				id,
				nome
			};
		} else {
			auxValor = '';
		}

		return {
			fabricantes: [],
			value: auxValor,
		}
	},
	methods: {
		retornaValor() {
			const {
				id,
				nome
			} = this.value;
			this.$emit('retornaValor', {
				id,
				nome
			});
		}
	},
	created() {
		fabricantesServiceComUrl
			.lista(res => this.fabricantes = JSON.parse(res));
	},
	template: `
	<div class="form-group">
	<label :for="htmlFor"> {{ label }} </label>
	<select :id="htmlFor" required class="custom-select" @change="retornaValor()" v-model="value">
		<option :selected="!value" disabled value=""> {{ labelFirstItem }}</option>
		<option v-for="fabricante in fabricantes" :key="fabricante.id" :value="fabricante">{{ fabricante.nome }}</option>
	</select>
	</div>
	`,
});

let FormInput = Vue.component('form-input', {
	template: `
	<div class="form-group row">
		<label :for="htmlFor" class="col-12 col-form-label">{{ label }}</label>
		<div class="col-12">
			<input :id="htmlFor" autocomplete="off" :required="required" :type="type" class="form-control" v-model.lazy="valor" @blur="retornaValor()" :placeholder="placeholder" />
		</div>
	</div>
	`,
	props: ['label', 'type', 'placeholder', 'htmlFor', 'valorInput', 'required'],
	data() {
		return {
			valor: this.valorInput
		}
	},
	methods: {
		retornaValor() {
			this.$emit('retornaValor', this.valor);
		}
	}
});


let CadastroProduto = Vue.component('componente-produto', {
	template: `
  <div class="row flex-wrap fix-height-row">
    <div class="col-md-12 col-lg-9 fix-height">
      <h2>Produtos</h2>
      <table class="table">
        <tr>
          <th>Nome</th>
          <th>Fabricante</th>
          <th>Volume</th>
          <th>Unidade</th>
          <th>Estoque</th>
          <th></th>
          <th></th>
        </tr>
        <tbody id="event-table">
          <tr v-for="produto in produtos" :key="produto.id">
            <td>{{ produto.nome }}</td>
            <td>{{ produto.fabricante.nome }}</td>
            <td>{{ produto.volume }}</td>
            <td>{{ produto.unidade }}</td>
            <td>{{ produto.estoque }}</td>
            <td>
              <span class="cursor-pointer" @click="alteraProduto(produto)">&#9998;</span>
            </td>
            <td>
              <span class="cursor-pointer" v-b-modal.modal-excluir @click="modalExcluirProduto(produto)">&#10006;</span>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="col-md-12 col-lg-3 d-flex justify-content-center align-items-start">
      <div class="ajusta-espaco" v-if="toogleButtonForm">
		<div class="ajusta-espaco">
		</div>
        <button class="btn btn-primary" @click="handleToogleButtonForm">Registrar Produto</button>
      </div>
      <div v-else>
        <form-cadastra-produto
          :produtoAlterar="produtoAlterar"
          :limparForm="true"
          @posGravacao="posGravacao()"
          @fecharCadastro="handleToogleButtonForm()"
        />
      </div>
		</div>
		<b-modal id="modal-excluir" okVariant="danger" @ok="excluirProduto()" title="Excluir produto?">
			<p class="my-4">Tem certeza de excluir {{confirmaExcluirProduto && confirmaExcluirProduto.nome}}?</p>
			<template slot="modal-ok" @click="excluirProduto()">
				Excluir
			</template>
	  </b-modal>
  </div>
		`,
	created() {
		produtoServiceComUrl
			.lista(res => this.produtos = JSON.parse(res));
	},
	data() {
		return {
			produtos: [],
			toogleButtonForm: true,
			produtoAlterar: null,
			confirmaExcluirProduto: null,
		}
	},
	methods: {
		handleToogleButtonForm() {
			this.produtoAlterar = null;
			this.toogleButtonForm = !this.toogleButtonForm;
		},
		excluirProduto() {
			produtoServiceComUrl
				.apaga(this.confirmaExcluirProduto.id, () => {
					this.posGravacao();
				});
		},
		modalExcluirProduto(produto) {
			this.confirmaExcluirProduto = produto;
		},
		alteraProduto(produto) {
			this.produtoAlterar = produto;
			this.toogleButtonForm = !this.toogleButtonForm;
		},
		posGravacao() {
			this.toogleButtonForm = true;
			produtoServiceComUrl
				.lista(res => this.produtos = JSON.parse(res));
		}
	}
});

let CadastroFabricante = Vue.component('componente-fabricante', {
	template: `
  <div class="row flex-wrap fix-height-row">
    <div class="col-md-12 col-lg-9 fix-height">
      <h4>Lista de Fabricantes</h4>
      <table class="table">
        <tr>
          <th>Nome</th>
          <th></th>
          <th></th>
        </tr>
        <tbody id="event-table">
          <tr v-for="fabricante in fabricantes" :key="fabricante.id">
            <td>{{ fabricante.nome }}</td>
            <td>
              <span class="cursor-pointer" @click="alterarFabricante(fabricante)">&#9998;</span>
            </td>
            <td>
              <span class="cursor-pointer" v-b-modal.modal-excluir @click="modalExcluirFabricante(fabricante)">&#10006;</span>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="col-md-12 col-lg-3 d-flex justify-content-center align-items-start">
      <div class="ajusta-espaco" v-if="toogleButtonForm">
		<div class="ajusta-espaco">
		</div>
        <button class="btn btn-primary" @click="handleToogleButtonForm()">Registrar Fabricante</button>
      </div>
      <div v-else>
				<form-cadastra-fabricante
					:fabricanteAlterar="fabricanteAlterar"
					:limparForm="true"
					@posGravacao="posGravacao()"
					@fecharCadastro="handleToogleButtonForm()"/> 
      </div>
		</div>
		<b-modal id="modal-excluir" okVariant="danger" @ok="excluirFabricante()" title="Excluir fabricante?">
			<p class="my-4">Tem certeza de excluir {{confirmaExcluirFabricante && confirmaExcluirFabricante.nome}}?</p>
			<template slot="modal-ok" @click="excluirFabricante()">
				Excluir
			</template>
		</b-modal>
		<b-alert variant="danger" :show="errorExclusao" dismissible>
			Não foi possivel excluir o fabricante, existem produtos associados!
		</b-alert>
  </div>
	`,
	created() {
		fabricantesServiceComUrl
			.lista(res => this.fabricantes = JSON.parse(res));
	},
	data() {
		return {
			fabricantes: [],
			toogleButtonForm: true,
			fabricanteAlterar: null,
			confirmaExcluirFabricante: null,
			errorExclusao: false,
		}
	},
	methods: {
		handleToogleButtonForm() {
			this.fabricanteAlterar = null;
			this.toogleButtonForm = !this.toogleButtonForm;
		},
		excluirFabricante() {
			fabricantesServiceComUrl
				.apaga(this.confirmaExcluirFabricante.id, (_, error) => {
					if (error){
						this.errorExclusao = true;
					}
					this.posGravacao();
			});
		},
		modalExcluirFabricante(fabricante) {
			this.confirmaExcluirFabricante = fabricante;
		},
		alterarFabricante(fabricante) {
			this.fabricanteAlterar = fabricante;
			this.toogleButtonForm = !this.toogleButtonForm;
		},
		posGravacao() {
			this.toogleButtonForm = true;
			fabricantesServiceComUrl
				.lista(res => this.fabricantes = JSON.parse(res));
		}
	}
});
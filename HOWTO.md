# Projeto mercado

Caro avaliador, o projeto foi feito usando o servidor Tomcat v9
Para executar, é necessário compilar com maven e fazer deploy em um servidor tomCat.
Também pode-se rodar pela IDE, no meu caso, utilizei a IDE eclipse. Inclui o servidor TomCat e adicionei o projeto ao servidor pela propria IDE.

Foi usado o banco de dados mysql, um database chamado mercado, com usuario chamado **root** e **sem senha**.

Os testes foram feitos com JUnit, estão localizados no diretorio src/test/java, e podem ser rodados do próprio eclipse ou com maven, com o servidor já levantado, pois os testes comprovam o funcionamento dos serviços.

A interface do usuario foi feita com vue conforme solicitado, como uma SPA (single page application), que registra tanto o fabricante quanto os produtos.
A página inicial esta localizada em **http://localhost:8080/mercado/#/**, que deve ser acessado quando o servidor estiver no ar e a aplicação estiver implantada.